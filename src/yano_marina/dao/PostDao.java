package yano_marina.dao;

import static yano_marina.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import yano_marina.beans.Post;
import yano_marina.exception.SQLRuntimeException;

public class PostDao {

    public void insert(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO posts ( ");
            sql.append("user_id");
            sql.append(", subject");
            sql.append(", text");
            sql.append(", category");
            sql.append(", post_date");
            sql.append(", deleted");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); //subject
            sql.append(", ?"); // text
            sql.append(", ?"); //category
            sql.append(", CURRENT_TIMESTAMP"); // post_date
            sql.append(", ?"); //deleted
            sql.append(", CURRENT_TIMESTAMP"); //updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, post.getUserId());
            ps.setString(2, post.getSubject());
            ps.setString(3, post.getText());
            ps.setString(4, post.getCategory());
            ps.setInt(5, post.getDeleted());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}