package yano_marina.dao;

import static yano_marina.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import yano_marina.beans.Comment;
import yano_marina.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("user_id");
            sql.append(", text");
            sql.append(", posts_no");
            sql.append(", deleted");
            sql.append(", post_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); // text
            sql.append(", ?"); //post_no
            sql.append(", ?"); //deleted
            sql.append(", CURRENT_TIMESTAMP"); // post_date
            sql.append(", CURRENT_TIMESTAMP"); //updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getUserId());
            ps.setString(2, comment.getText());
            ps.setInt(3, comment.getPostsNo());
            ps.setInt(4, comment.getDeleted());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}