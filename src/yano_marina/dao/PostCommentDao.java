package yano_marina.dao;

import static yano_marina.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import yano_marina.beans.PostComment;
import yano_marina.exception.NoRowsUpdatedRuntimeException;
import yano_marina.exception.SQLRuntimeException;

public class PostCommentDao {

	public List<PostComment> getPostComments(Connection connection, int num){

		PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.no as no, ");
            sql.append("comments.user_id as user_id, ");
            sql.append("users.id as id, ");
            sql.append("users.name as name, ");
            sql.append("comments.text as text, ");
            sql.append("comments.post_date as post_date, ");
            sql.append("comments.posts_no as posts_no, ");
            sql.append("comments.deleted as deleted, ");
            sql.append("comments.updated_date as updated_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY post_date DESC limit " + num);


            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<PostComment> ret = toPostCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
	}

	private List<PostComment> toPostCommentList(ResultSet rs)
			throws SQLException {

		List<PostComment> ret = new ArrayList<PostComment>();
        try {
            while (rs.next()) {
            	int no = rs.getInt("no");
            	int userId =rs.getInt("user_id");
            	String name =rs.getString("name");
                String text = rs.getString("text");
                Timestamp postDate = rs.getTimestamp("post_date");
                int postsNo = rs.getInt("posts_no");
                int deleted = rs.getInt("deleted");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                PostComment comment = new PostComment();
                comment.setNo(no);
                comment.setUserId(userId);
                comment.setName(name);
                comment.setText(text);
                comment.setPost_date(postDate);
                comment.setPostsNo(postsNo);
                comment.setDeleted(deleted);
                comment.setUpdate_date(updatedDate);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }

	}

	public void stop(Connection connection, int commentDelete, int no) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE comments SET");
			sql.append("  deleted = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" no = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, commentDelete);
			ps.setInt(2, no);

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

}
