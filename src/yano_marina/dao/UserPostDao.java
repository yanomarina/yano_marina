package yano_marina.dao;

import static yano_marina.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import yano_marina.beans.UserPost;
import yano_marina.exception.NoRowsUpdatedRuntimeException;
import yano_marina.exception.SQLRuntimeException;

public class UserPostDao {

    public List<UserPost> getUserPosts(Connection connection,
    		String categorysearch, String startdate, String enddate, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.no as no, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("users.id as id, ");
            sql.append("users.name as name, ");
            sql.append("posts.subject as subject, ");
            sql.append("posts.text as text, ");
            sql.append("posts.category as category, ");
            sql.append("posts.post_date as post_date, ");
            sql.append("posts.deleted as deleted, ");
            sql.append("posts.updated_date as updated_date ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            if(!(StringUtils.isEmpty(categorysearch))) {
            	sql.append("WHERE posts.category LIKE "+"'%"+categorysearch+"%'");
            }
            if(!(StringUtils.isEmpty(startdate))) {
            	sql.append("AND posts.post_date between" + "'"+startdate+"'"
            			+ "AND" + "'2030-01-01'");
            }
            if(!(StringUtils.isEmpty(enddate))) {
            	sql.append("AND posts.post_date <= " + "'"+enddate+"23:59:59'");
            }
            sql.append("ORDER BY post_date DESC limit " + num);


            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserPostList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserPost> toUserPostList(ResultSet rs)
            throws SQLException {

        List<UserPost> ret = new ArrayList<UserPost>();
        try {
            while (rs.next()) {
            	int no = rs.getInt("no");
            	int userId =rs.getInt("user_id");
            	String name =rs.getString("name");
                String subject = rs.getString("subject");
                String text = rs.getString("text");
                String category = rs.getString("category");
                Timestamp postDate = rs.getTimestamp("post_date");
                int deleted = rs.getInt("deleted");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                UserPost post = new UserPost();
                post.setNo(no);
                post.setUserId(userId);
                post.setName(name);
                post.setSubject(subject);
                post.setText(text);
                post.setCategory(category);
                post.setPost_date(postDate);
                post.setDeleted(deleted);
                post.setUpdatedDate(updatedDate);

                ret.add(post);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

	public void stop(Connection connection, int postDelete, int no) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE posts SET");
			sql.append("  deleted = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" no = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, postDelete);
			ps.setInt(2, no);

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}


}