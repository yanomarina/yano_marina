package yano_marina.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import yano_marina.beans.User;
import yano_marina.exception.NoRowsUpdatedRuntimeException;
import yano_marina.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	User user = (User) request.getSession().getAttribute("loginUser");
    	List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

    	if(user == null) {
    		session.setAttribute("errorMessages", messages);
    		messages.add("ログインしてください");
    		response.sendRedirect("./");
    	}else if(user != null){
    		int department = user.getDepartment();
    		if(department != 1) {
    			session.setAttribute("errorMessages", messages);
        		messages.add("権限がありません");
        		response.sendRedirect("./");
    		}else if(department == 1){
    			request.getRequestDispatcher("settings.jsp").forward(request, response);
    		}
    	}
    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);


		if (isValid(request, messages) == true) {

			try {
				String onPassword = request.getParameter("password");
				int id = Integer.parseInt(request.getParameter("id"));

				User user = new UserService().getUser(id);
				String password = user.getPassword();

				if (StringUtils.isEmpty(onPassword)) {
					editUser.setPassword(password);
					new UserService().update2(editUser);
				}else {
					editUser.setPassword(onPassword);
					new UserService().update(editUser);
				}

			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			session.setAttribute("User", editUser);

			response.sendRedirect("management");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setName(request.getParameter("name"));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
		editUser.setDepartment(Integer.parseInt(request.getParameter("department")));
		editUser.setStopRevival(Integer.parseInt(request.getParameter("stopRevival")));
		return editUser;
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int department = Integer.parseInt(request.getParameter("department"));

		if (StringUtils.isEmpty(name) == true) {
			messages.add("ユーザー名を入力してください");
		}else if (name.length() > 10 ) {
			messages.add("ユーザー名は10文字以下で入力してください");
		}
		if (StringUtils.isEmpty(loginId) == true ) {
			messages.add("ログインIDを入力してください");
		}else if (loginId.length() < 6 ) {
			messages.add("ログインIDは６文字以上で入力してください");
		}else if (20 < loginId.length()) {
			messages.add("ログインIDは２０文字以下で入力してください");
		}else if (!loginId.matches("[0-9a-zA-Z]+")) {
			messages.add("ログインIDは半角英数字で入力してください");
		}
		if (StringUtils.isEmpty(password) == false && password.length() < 6 ) {
			messages.add("パスワードは６文字以上で入力してください");
		}else if (StringUtils.isEmpty(password) == false && 20 < password.length()) {
			messages.add("パスワードは２０文字以下で入力してください");
		}
		if (!password.equals(passwordCheck)) {
			messages.add("パスワードと確認用パスワードが一致しません");
		}
		if(branch == 1 && (department == 3 || department == 4)) {
			messages.add("支店と部署・役職の組み合わせが不正です");
		}else if(branch != 1 && (department == 1 || department == 2)) {
			messages.add("支店と部署・役職の組み合わせが不正です");
		}

		int id = Integer.parseInt(request.getParameter("id"));
		User user = new UserService().getUser(id);
		String nowLoginId = user.getLoginId();
		User user2 = new UserService().getUser(loginId);

		if((!loginId.equals(nowLoginId)) && (user2 != null)) {
			messages.add("このIDは使用されています");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}