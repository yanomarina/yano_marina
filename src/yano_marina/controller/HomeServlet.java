package yano_marina.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import yano_marina.beans.PostComment;
import yano_marina.beans.User;
import yano_marina.beans.UserPost;
import yano_marina.service.CommentService;
import yano_marina.service.PostService;


@WebServlet(urlPatterns = {"/index.jsp"})
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

        User user = (User) request.getSession().getAttribute("loginUser");
        HttpSession session = request.getSession();
        List<String> messages = new ArrayList<String>();

        if(user == null) {
        	session.setAttribute("errorMessages", messages);
    		messages.add("ログインしてください");
        	request.getRequestDispatcher("login.jsp").forward(request, response);
        }

        if(user != null) {
        	List<UserPost> posts = new PostService().getPost((request.getParameter("categorysearch")),
        		(request.getParameter("startdate")), (request.getParameter("enddate")));

        	String categorysearch = request.getParameter("categorysearch");
        	String startdate = request.getParameter("startdate");
        	String enddate = request.getParameter("enddate");

        	if(StringUtils.isEmpty(categorysearch) == false) {
        		request.setAttribute("categorysearch", categorysearch);
        	}
        	if(StringUtils.isEmpty(startdate) == false) {
        		Date startDate = java.sql.Date.valueOf(startdate);
        		request.setAttribute("startdate", startDate);
        	}
        	if(StringUtils.isEmpty(enddate) == false) {
        		Date endDate = java.sql.Date.valueOf(enddate);
        		request.setAttribute("enddate", endDate);
        	}

        	request.setAttribute("posts", posts);

        	List<PostComment> comments = new CommentService().getComment();

        	request.setAttribute("comments", comments);

        	request.getRequestDispatcher("/home.jsp").forward(request, response);
        }
	}


}

