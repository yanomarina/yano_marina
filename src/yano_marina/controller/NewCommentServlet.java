package yano_marina.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import yano_marina.beans.Comment;
import yano_marina.beans.User;
import yano_marina.service.CommentService;


@WebServlet(urlPatterns = { "/newComment" })
public class NewCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		User user = (User) request.getSession().getAttribute("loginUser");
        HttpSession session = request.getSession();
        List<String> messages = new ArrayList<String>();

		if(user == null) {
        	session.setAttribute("errorMessages", messages);
    		messages.add("ログインしてください");
        	request.getRequestDispatcher("login.jsp").forward(request, response);
        }else {
        	request.getRequestDispatcher("./").forward(request, response);
        }

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Comment comment = new Comment();
            comment.setPostsNo(Integer.parseInt(request.getParameter("postNo")));
            comment.setText(request.getParameter("comment"));
            comment.setDeleted(Integer.parseInt(request.getParameter("commentDeleted")));
            comment.setUserId(user.getId());

            new CommentService().register(comment);

            response.sendRedirect("./");
        } else {

            session.setAttribute("errorMessages", messages);
            response.sendRedirect("./");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String comment = request.getParameter("comment");

        if (StringUtils.isEmpty(comment) == true) {
            messages.add("コメントを入力してください");
        }
        if (500 < comment.length()) {
            messages.add("本文は500文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }



}
