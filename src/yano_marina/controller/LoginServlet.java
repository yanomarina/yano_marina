package yano_marina.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import yano_marina.beans.User;
import yano_marina.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

    	request.getRequestDispatcher("login.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	String loginId = request.getParameter("login_id");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        User user = loginService.login(loginId, password);
        HttpSession session = request.getSession();

        if (user != null) {
        	int stopRevival = user.getStopRevival();
        	if(stopRevival == 0) {
        		session.setAttribute("loginUser", user);
        		response.sendRedirect("./");
        	}else {
                List<String> messages = new ArrayList<String>();
                messages.add("ログインIDまたはパスワードが誤っています");

                session.setAttribute("errorMessages", messages);
                request.setAttribute("errorLoginId", loginId);

                request.getRequestDispatcher("login.jsp").forward(request, response);
        	}
        } else {
            List<String> messages = new ArrayList<String>();

            if(StringUtils.isEmpty(loginId) == true || StringUtils.isEmpty(password) == true) {
            	messages.add("ログインIDまたはパスワードが入力されていません");
            }else {
            	messages.add("ログインIDまたはパスワードが誤っています");
            }

            session.setAttribute("errorMessages", messages);
            request.setAttribute("errorLoginId", loginId);

            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }
}