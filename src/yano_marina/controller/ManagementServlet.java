package yano_marina.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import yano_marina.beans.BranchDepartmentUser;
import yano_marina.beans.User;
import yano_marina.service.UserService;


@WebServlet(urlPatterns = {"/management"})
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		User user = (User) request.getSession().getAttribute("loginUser");
    	List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

    	if(user == null) {
    		session.setAttribute("errorMessages", messages);
    		messages.add("ログインしてください");
    		response.sendRedirect("./");
    	}else if(user != null){
    		int department = user.getDepartment();
    		if(department != 1) {
    			session.setAttribute("errorMessages", messages);
        		messages.add("権限がありません");
        		response.sendRedirect("./");
    		}else if(department == 1){
    			List<BranchDepartmentUser> users = new UserService().getUsers();
    			request.setAttribute("users", users);
    			request.getRequestDispatcher("/management.jsp").forward(request, response);
    		}
    	}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if(request.getParameter("stop") != null) {
			new UserService().stop(
					(Integer.parseInt(request.getParameter("stop"))),
					(Integer.parseInt(request.getParameter("id"))));
			response.sendRedirect("management");
		}
		if(request.getParameter("revival") != null) {
			new UserService().stop(
					(Integer.parseInt(request.getParameter("revival"))),
					(Integer.parseInt(request.getParameter("id"))));
			response.sendRedirect("management");
		}

	}



}
