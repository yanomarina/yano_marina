package yano_marina.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import yano_marina.service.CommentService;


@WebServlet(urlPatterns = {"/deleteComment"})
public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if(request.getParameter("commentDelete") != null) {

			int commentDelete = Integer.parseInt(request.getParameter("commentDelete"));
			int no = Integer.parseInt(request.getParameter("no"));

			new CommentService().stop(commentDelete, no);

			response.sendRedirect("./");
		}

	}

}
