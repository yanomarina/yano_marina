package yano_marina.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import yano_marina.beans.User;
import yano_marina.service.UserService;

@WebServlet(urlPatterns = {"/signup"})
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("loginUser");
    	List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

    	if(user == null) {
    		session.setAttribute("errorMessages", messages);
    		messages.add("ログインしてください");
    		response.sendRedirect("./");
    	}else if(user != null){
    		int department = user.getDepartment();
    		if(department != 1) {
    			session.setAttribute("errorMessages", messages);
        		messages.add("使用権限がありません");
        		response.sendRedirect("./");
    		}else if(department == 1){
    			request.getRequestDispatcher("signup.jsp").forward(request, response);
    		}
    	}
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = new User();

			user.setLoginId(request.getParameter("loginId"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch(Integer.parseInt(request.getParameter("branch")));
			user.setDepartment(Integer.parseInt(request.getParameter("department")));
			user.setStopRevival(Integer.parseInt(request.getParameter("stopRevival")));
			new UserService().register(user);

			response.sendRedirect("management");
		}else {
			String loginId = request.getParameter("loginId");
			String name = request.getParameter("name");
			int branch = Integer.parseInt(request.getParameter("branch"));
			int department = Integer.parseInt(request.getParameter("department"));

			session.setAttribute("errorMessages", messages);
			request.setAttribute("errorLoginId", loginId);
			request.setAttribute("errorName", name);
			request.setAttribute("errorBranch", branch);
			request.setAttribute("errorDepartment", department);

			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}

	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int department = Integer.parseInt(request.getParameter("department"));

		if (StringUtils.isEmpty(name) == true) {
			messages.add("ユーザー名を入力してください");
		}else if (name.length() > 10 ) {
			messages.add("ユーザー名は10文字以下で入力してください");
		}
		if (StringUtils.isEmpty(loginId) == true ) {
			messages.add("ログインIDを入力してください");
		}else if (loginId.length() < 6 ) {
			messages.add("ログインIDは６文字以上で入力してください");
		}else if (20<loginId.length()) {
			messages.add("ログインIDは２０文字以下で入力してください");
		}else if (!loginId.matches("[0-9a-zA-Z]+")) {
			messages.add("IDは半角英数字で入力してください");
		}
		if (StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}else if (password.length() < 6 ) {
			messages.add("パスワードは６文字以上で入力してください");
		}else if (20 < password.length()) {
			messages.add("パスワードは２０文字以下で入力してください");
		}
		if(!password.equals(passwordCheck)) {
			messages.add("入力したパスワードと確認用パスワードが一致しません");
		}
		if(branch == 1 && (department == 3 || department == 4)) {
			messages.add("支店と部署・役職の組み合わせが不正です");
		}else if(branch != 1 && (department == 1 || department == 2)) {
			messages.add("支店と部署・役職の組み合わせが不正です");
		}


		User user = new UserService().getUser(loginId);
		if(user != null) {
			messages.add("このIDは使用されています");
		}

		if (messages.size() == 0) {
			return true;
		}else {
			return false;
		}
	}

}
