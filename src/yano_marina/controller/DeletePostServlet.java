package yano_marina.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import yano_marina.service.PostService;


@WebServlet(urlPatterns = {"/deletePost"})
public class DeletePostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if(request.getParameter("postDelete") != null) {

			int postDelete = Integer.parseInt(request.getParameter("postDelete"));
			int no = Integer.parseInt(request.getParameter("no"));

			new PostService().stop(postDelete, no);

			response.sendRedirect("./");
		}




	}

}
