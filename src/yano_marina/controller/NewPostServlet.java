package yano_marina.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import yano_marina.beans.Post;
import yano_marina.beans.User;
import yano_marina.service.PostService;


@WebServlet(urlPatterns = {"/newPost"})
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		User user = (User) request.getSession().getAttribute("loginUser");
    	List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if(user == null) {
			session.setAttribute("errorMessages", messages);
			messages.add("ログインしてください");
			response.sendRedirect("./");
    	}else if(user != null){
    		request.getRequestDispatcher("newpost.jsp").forward(request, response);
    	}
	}


	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if(isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Post post = new Post();
			post.setSubject(request.getParameter("subject"));
			post.setText(request.getParameter("text"));
			post.setCategory(request.getParameter("category"));
			post.setDeleted(Integer.parseInt(request.getParameter("postDeleted")));
			post.setUserId(user.getId());

			new PostService().register(post);

			response.sendRedirect("./");
		}else {
			String subject = request.getParameter("subject");
			String text =request.getParameter("text");
			String category = request.getParameter("category");

			session.setAttribute("errorMessages", messages);
			request.setAttribute("errorSubject", subject);
			request.setAttribute("errorText", text);
			request.setAttribute("errorCategory", category);
			request.getRequestDispatcher("newpost.jsp").forward(request, response);
		}
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String subject = request.getParameter("subject");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if (StringUtils.isEmpty(subject) == true) {
			messages.add("件名を入力してください");
		}else if (30 < subject.length()) {
			messages.add("件名は30字以下で入力してください");
		}
		if (StringUtils.isEmpty(text) == true) {
			messages.add("本文を入力してください");
		}else if (1000 < text.length()) {
			messages.add("本文は1000文字以内で入力してください");
		}
		if (StringUtils.isEmpty(category) == true) {
			messages.add("カテゴリを入力してください");
		}else if (10 < category.length()) {
			messages.add("カテゴリは10文字以下で入力してください");
		}

		if(messages.size() == 0) {
			return true;
		}else {
			return false;
		}

	}

}
