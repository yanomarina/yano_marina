package yano_marina.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import yano_marina.beans.User;
import yano_marina.service.UserService;


@WebServlet(urlPatterns = {"/edit"})
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("edit")));
		request.setAttribute("editUser", editUser);
		request.getRequestDispatcher("/settings.jsp").forward(request, response);
	}

}
