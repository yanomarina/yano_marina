package yano_marina.service;

import static yano_marina.utils.CloseableUtil.*;
import static yano_marina.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import yano_marina.beans.Comment;
import yano_marina.beans.PostComment;
import yano_marina.dao.CommentDao;
import yano_marina.dao.PostCommentDao;


public class CommentService {

    public void register(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

	public List<PostComment> getComment() {

		Connection connection = null;
		try {
			connection = getConnection();

			PostCommentDao commentDao = new PostCommentDao();
			List<PostComment> ret = commentDao.getPostComments(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void stop(int commentDelete, int no) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostCommentDao postCommentDao = new PostCommentDao();
			postCommentDao.stop(connection, commentDelete, no);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}


}