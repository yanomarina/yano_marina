package yano_marina.service;

import static yano_marina.utils.CloseableUtil.*;
import static yano_marina.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import yano_marina.beans.Post;
import yano_marina.beans.UserPost;
import yano_marina.dao.PostDao;
import yano_marina.dao.UserPostDao;


public class PostService {

    public void register(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.insert(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<UserPost> getPost(String categorysearch, String startdate, String enddate) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserPostDao postDao = new UserPostDao();
    		List<UserPost> ret = postDao.getUserPosts(connection, categorysearch, startdate, enddate, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

	public void stop(int postDelete, int no) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserPostDao userPostDao = new UserPostDao();
			userPostDao.stop(connection, postDelete, no);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}





}