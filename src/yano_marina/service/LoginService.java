package yano_marina.service;

import static yano_marina.utils.CloseableUtil.*;
import static yano_marina.utils.DBUtil.*;

import java.sql.Connection;

import yano_marina.beans.User;
import yano_marina.dao.UserDao;
import yano_marina.utils.CipherUtil;

public class LoginService {

	public User login(String login_id, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, login_id, encPassword);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}