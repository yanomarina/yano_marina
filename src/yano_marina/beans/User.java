package yano_marina.beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;


    private int id;
    private String login_id;
    private String password;
    private String name;
    private int branch;
    private int department;
    private int stop_revival;
    private Date updated_date;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return login_id;
	}
	public void setLoginId(String login_id) {
		this.login_id = login_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranch() {
		return branch;
	}
	public void setBranch(int branch) {
		this.branch = branch;
	}
	public int getDepartment() {
		return department;
	}
	public void setDepartment(int department) {
		this.department = department;
	}
	public int getStopRevival() {
		return stop_revival;
	}
	public void setStopRevival(int stop_revival) {
		this.stop_revival = stop_revival;
	}
	public Date getUpdatedDate() {
		return updated_date;
	}
	public void setUpdatedDate(Date updated_date) {
		this.updated_date = updated_date;
	}

}