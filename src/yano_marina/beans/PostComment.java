package yano_marina.beans;

import java.io.Serializable;
import java.util.Date;

public class PostComment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int no;
    private int user_id;
    private String name;
    private String text;
    private Date post_date;
    private int posts_no;
    private int deleted;
    private Date update_date;

	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public int getUserId() {
		return user_id;
	}
	public void setUserId(int user_id) {
		this.user_id = user_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Date getPost_date() {
		return post_date;
	}
	public void setPost_date(Date post_date) {
		this.post_date = post_date;
	}
	public int getPostsNo() {
		return posts_no;
	}
	public void setPostsNo(int posts_no) {
		this.posts_no = posts_no;
	}
	public int getDeleted() {
		return deleted;
	}
	public void setDeleted(int deleted) {
		this.deleted = deleted;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

}