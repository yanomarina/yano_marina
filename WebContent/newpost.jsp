<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
		 <c:if test="${ not empty errorMessages }">
		 	<div class="errorMessages">
		 		<ul>
		 			<c:forEach items="${errorMessages}" var="message">
		 				<li><c:out value="${message}" />
		 			</c:forEach>
		 		</ul>
		 	</div>
		 	<c:remove var="errorMessages" scope="session" />
		 </c:if>

		</div>
		<div class="header">
			<c:if test="${ not empty loginUser }">
				<a href="./">ホームに戻る</a>
				<div class="form-area">
					<p>新規投稿</p>
					<form action="newPost" method="post">
					件名<br/>
					<input type="text" value="${errorSubject}" name="subject"><br/>
					本文<br/>
					<textarea name="text" rows="30" cols="70" class="text">${errorText}</textarea><br/>
					カテゴリー<br/>
					<input type="text" value="${errorCategory}" name="category"><br/>
					<input type="hidden" value="0" name="postDeleted" id="postDeleted">
					<p>
					<input type="submit" value="投稿" id="post_button"><br/>
					</form>
				</div>
				<div class="copylight"> Copyright(c)YanoMarina</div>
			</c:if>
		</div>
	</body>
</html>