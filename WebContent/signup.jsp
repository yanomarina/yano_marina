<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー新規登録</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
                <br />
                <label for="name">ユーザー名</label>
                <input name="name" value="${errorName}" id="name" /><br />

                <label for="loginId">ログインID</label>
                <input name="loginId" value="${errorLoginId}" id="loginId" /> <br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password" /> <br />

                <label for="passwordCheck">パスワード確認</label>
                <input name="passwordCheck" type="password" id="passwordCheck" /> <br />

                <label for="branch">支店</label>
                <select name="branch" id="branch" >
                <option value="1" <c:if test="${errorBranch == 1}">selected</c:if>>本社</option>
                <option value="2" <c:if test="${errorBranch == 2}">selected</c:if>>支店Ａ</option>
                <option value="3" <c:if test="${errorBranch == 3}">selected</c:if>>支店Ｂ</option>
                <option value="4" <c:if test="${errorBranch == 4}">selected</c:if>>支店Ｃ</option>
                </select>
                <br/>

                <label for="department">部署・役職</label>
                <select name="department" id="department" >
                <option value="1" <c:if test="${errorDepartment == 1}">selected</c:if>>人事総務部</option>
                <option value="2" <c:if test="${errorDepartment == 2}">selected</c:if>>情報管理担当者</option>
                <option value="3" <c:if test="${errorDepartment == 3}">selected</c:if>>支店長</option>
                <option value="4" <c:if test="${errorDepartment == 4}">selected</c:if>>社員</option>
                </select>
                <br />

                <input name="stopRevival" value="0" id="stopRevival" type="hidden">

                <br /> <input type="submit" value="新規登録" /> <br />

                <a href="management">戻る</a>
            </form>

            <div class="copyright">Copyright(c)Yano</div>
        </div>

	</body>
</html>