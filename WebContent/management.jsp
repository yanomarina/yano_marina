<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー管理</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">

	</head>
	<body>
		<div class="main-contents">
			<div class="header">
					<a href="./">ホームに戻る</a><br>
					<a href="signup">新規登録</a>
			</div>
			<div class="management-table">
				<table border="1">
					<tr>
						<th>編集</th>
						<th>停止・復活</th>
						<th>ログインID</th>
						<th>ユーザー名</th>
						<th>支店</th>
						<th>部署・役職</th>
					</tr>
					<c:forEach items="${users}" var="users">
					<tr>
						<td>
						<form method="post" action="edit">
							<button name="edit" type="submit" value="${users.id}" id="edit_button">編集</button>
						</form>
						</td>
						<td>
						<form method="post" action="management">
							<c:if test="${users.stopRevival == 0}">
								<input name="id" type="hidden" value="${users.id}" />
								<input name="stop" type="hidden" value="1">
								<input type="submit" value="停止" onClick="return confirm('アカウントを停止します')" id="stop_button">
							</c:if>
							<c:if test="${users.stopRevival == 1}">
								<input name="id" type="hidden" value="${users.id}" />
								<input name="revival" type="hidden" value="0">
								<input type="submit" value="復活" onClick="return confirm('アカウントを復活します')" id="revival_button">
							</c:if>
						</form>
						</td>
						<td><c:out value="${users.loginId}"/></td>
						<td><c:out value="${users.name}"/></td>
						<td><c:out value="${users.branchName}"/></td>
						<td><c:out value="${users.departmentName}"/></td>
					</tr>
					</c:forEach>
				</table>
			</div>
		</div>
		<div class="copylight"> Copyright(c)YanoMarina</div>

	</body>
</html>