<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板システム</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

			<div class="header">
				<c:if test="${ empty loginUser }">
					<a href="login">ログイン</a>
				</c:if>
				<c:if test="${ not empty loginUser }">
					<a href="newPost">新規投稿</a><br>
					<a href="management">ユーザー管理画面</a><br>
					<form action="./" method="get">
						カテゴリー検索：<input type="text" size="15" name="categorysearch" value="${categorysearch}">
						<input type="submit" value="検索" id="search"><br>
						投稿日時検索 ：<input type="date" size="15" name="startdate" value="${startdate}">～
						<input type="date" size="15" name="enddate" value="${enddate}">
						<input type="submit" value="検索" id="search"><br>
					</form>
					<a href="logout">ログアウト</a>
				</c:if>
			</div>
			<c:if test="${ not empty loginUser }">
				<div class="profile">
					<div class="name"><h2><c:out value="${loginUser.name}" />のページ</h2></div>
				</div>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<div class="posts">
					<div><h3>投稿一覧</h3></div>
					<c:forEach items="${posts}" var="post">
					<c:if test="${ post.deleted == 0}">
						<p>------------------------------------------------------</p>
						<div class="subject"><p>【件名】<c:out value="${post.subject}" /></div>
						<div class="text"><c:out value="${post.text}" /></div>
						<div class="category"><p>【カテゴリー】<c:out value="${post.category}" /></div>
						<div class="date"><p>【投稿日時】<fmt:formatDate value="${post.post_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
						<div class="name"><p>【投稿者】<c:out value="${post.name}" /></div>
						<c:if test="${post.userId == loginUser.id}">
							<form method="post" action="deletePost">
								<input name="no" type="hidden" value="${post.no}">
								<input name="postDelete" type="hidden" value="1">
								<input type="submit" value="投稿削除" id="post_delete_button">
							</form>
						</c:if>
						<p>
						<form action="newComment" method="post">
							コメントする<br />
							<textarea name="comment" cols="50" rows="2" class="comment-box"></textarea>
							<br />
							<input name="postNo" type="hidden" value="${post.no}">
							<input name="commentDeleted" type="hidden" value="0" id="commentDeleted">
							<input type="submit" value="送信する" id="new_comment_button">
						</form>
						<c:forEach items="${comments}" var="comment">
							<div class="comment">
							<c:if test="${comment.postsNo == post.no}">
								<c:if test="${comment.deleted == 0}">
									<p>【コメント】</p>
									<div class="commentname"><c:out value="${comment.name}"/>　さんより</div>
									<div class="comment"><p><c:out value="${comment.text}"/></div>
									<c:if test="${comment.userId == loginUser.id}">
										<form method="post" action="deleteComment">
											<input name="no" type="hidden" value="${comment.no}">
											<input name="commentDelete" type="hidden" value="1">
											<input type="submit" value="コメント削除" id="comment_delete_button">
										</form>
									</c:if>
								</c:if>
							</c:if>
							</div>
						</c:forEach>
					</c:if>
					</c:forEach>
				</div>
			</c:if>
			<div class="copylight"> Copyright(c)YanoMarina</div>
		</div>

</body>
</html>