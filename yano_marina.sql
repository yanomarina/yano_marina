-- MySQL dump 10.13  Distrib 5.6.11, for Win32 (x86)
--
-- Host: localhost    Database: yano_marina
-- ------------------------------------------------------
-- Server version	5.6.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `branches` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branches`
--

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
INSERT INTO `branches` VALUES (1,'本社'),(2,'支店Ａ'),(3,'支店Ｂ'),(4,'支店Ｃ');
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `text` varchar(500) NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `posts_no` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,2,'昼に一瞬だけ大雨でしたね。','2019-09-06 00:56:55',NULL,0,'2019-09-09 05:51:18'),(2,2,'今日はどうですか？','2019-09-06 01:04:56',NULL,0,'2019-09-09 05:51:18'),(3,2,'あいうえお','2019-09-06 01:10:03',NULL,0,'2019-09-09 05:51:18'),(4,2,'あいうえお','2019-09-06 01:14:37',NULL,0,'2019-09-09 05:51:18'),(5,2,'かきくけこ','2019-09-06 01:17:48',NULL,0,'2019-09-09 05:51:18'),(6,2,'あいうえお','2019-09-06 01:24:25',8,0,'2019-09-09 05:51:18'),(7,2,'あいうえお','2019-09-06 02:14:00',4,0,'2019-09-09 05:51:18'),(8,1,'aaa','2019-09-06 08:16:56',6,0,'2019-09-09 05:51:18'),(9,1,'このコメントは消せるかな','2019-09-09 08:18:22',10,1,'2019-09-10 00:37:27'),(10,1,'コメント投稿テスト','2019-09-10 00:22:46',10,1,'2019-09-10 00:37:23'),(11,1,'コメント削除テスト','2019-09-10 00:32:13',10,1,'2019-09-10 00:37:05'),(12,1,'投稿削除コメントテスト','2019-09-10 00:43:31',10,0,'2019-09-10 00:43:31'),(13,1,'あああ','2019-09-12 02:04:29',11,1,'2019-09-12 02:04:35'),(14,1,'ああああ','2019-09-12 02:25:05',11,1,'2019-09-12 02:25:08'),(15,19,'テストてすと','2019-09-12 02:56:40',11,1,'2019-09-12 02:56:45'),(16,1,'aaaaaaaaaa','2019-09-12 05:07:53',14,1,'2019-09-12 05:08:04'),(17,1,'aaaaaa','2019-09-12 05:29:38',14,0,'2019-09-12 05:29:38'),(18,1,'aaaaaaaa','2019-09-12 06:01:29',11,0,'2019-09-12 06:01:29'),(19,1,'test','2019-09-12 06:18:50',14,0,'2019-09-12 06:18:50'),(20,1,'aaaaaaaaa','2019-09-12 06:19:01',14,1,'2019-09-12 06:19:12'),(21,4,'テスト','2019-09-12 08:32:15',15,1,'2019-09-12 08:36:31'),(22,1,'テスト','2019-09-13 00:36:16',15,1,'2019-09-13 00:36:48'),(23,1,'あああ','2019-09-13 07:55:17',16,0,'2019-09-13 07:55:17'),(24,4,'aaa','2019-09-17 02:55:25',16,1,'2019-09-17 02:56:13'),(25,4,'あああ','2019-09-17 04:22:46',16,0,'2019-09-17 04:22:46'),(26,2,'こんにちは。','2019-09-18 02:44:55',16,1,'2019-09-18 02:55:48');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'人事総務部'),(2,'情報管理担当者'),(3,'店長'),(4,'社員');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `subject` varchar(30) NOT NULL,
  `text` varchar(1000) NOT NULL,
  `category` varchar(10) NOT NULL,
  `post_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (4,1,'初めての投稿','１件目の投稿です。','テスト','2019-08-29 02:59:14',0,'2019-09-09 05:52:09'),(5,2,'2回目の投稿','こんにちは。\r\n今日は雨のち晴れです。','テスト','2019-08-29 04:37:59',0,'2019-09-09 05:52:09'),(6,2,'Aです。','いい天気です。\r\n明日も晴れると良いな。','天気','2019-08-29 04:58:31',0,'2019-09-09 05:52:09'),(7,1,'今日は雨','天気予報は曇りだったのに雨が降っています。','天気','2019-09-02 02:54:53',0,'2019-09-09 05:52:09'),(8,1,'今日は','朝晴れてうれしい。\r\nでも午後は雨かも。','天気','2019-09-03 01:40:08',0,'2019-09-09 08:37:53'),(9,1,'９月','9月になってあっという間に１週間。','感想','2019-09-09 06:57:27',0,'2019-09-10 00:22:09'),(10,1,'テスト','投稿削除テスト','テスト','2019-09-09 08:17:54',1,'2019-09-10 00:43:38'),(11,1,'投稿テスト','投稿テスト','テスト','2019-09-10 08:31:42',0,'2019-09-10 08:31:42'),(12,1,'投稿テスト','投稿テスト２','テスト','2019-09-11 00:31:57',1,'2019-09-11 05:10:25'),(13,1,'今日は','今日は曇りです','天気','2019-09-12 02:04:52',1,'2019-09-12 02:06:04'),(14,19,'初めての投稿','はじめまして','テスト','2019-09-12 02:56:07',0,'2019-09-12 02:56:07'),(15,1,'投稿テスト','test','テスト','2019-09-12 06:20:18',1,'2019-09-13 06:18:05'),(16,1,'投稿テスト','テスト','テスト','2019-09-13 00:38:13',0,'2019-09-13 00:38:13'),(17,4,'投稿テスト','投稿テスト','テスト','2019-09-17 02:55:47',1,'2019-09-17 02:56:04'),(18,2,'投稿テスト','投稿テスト','テスト','2019-09-18 02:57:09',0,'2019-09-18 02:57:09');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_id` varchar(20) NOT NULL,
  `name` varchar(10) NOT NULL,
  `password` varchar(255) NOT NULL,
  `branch` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `stop_revival` int(11) DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_id` (`login_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'0000001','矢野 まりな','5GJAcUtds6I-7mBHmmI--6TWM9J_5PA8kEueIZp_vmA',1,1,0,'2019-09-10 07:38:10'),(2,'0000002','Ａ','6kFaYb0ZkVCENmoKL9rr4HCpwxaId-zbXjb0kFtfiqM',4,3,0,'2019-09-18 05:04:34'),(3,'0000003','山田花子','zU86-mmCk34fzCg8RR8c2dNo6Zi1VP97Buq-bnI6OcI',1,2,0,'2019-09-06 08:11:49'),(4,'0000004','間宮祥太朗','uiSiiQIo73ChYlm7m3L7G0KtXynXxgoCmEVq207hFzc',3,3,0,'2019-09-17 04:33:37'),(5,'0000005','Ｂ','PeRyBedys502m4EajMUVoxzzEFEb8fRSm3I0mIUJ2lU',3,4,0,'2019-09-17 06:02:13'),(12,'0000006','Ｃ','J7LBDNJCGyTAA2MRHsf7Y1q-c7e_Lau5MlWDXwot26s',2,4,1,'2019-09-13 04:27:51'),(13,'0000007','雨男　晴子','heRREG-0CVRNDtynDwMLvpBe17fR3pPKIOU5Cgp_P9U',1,1,0,'2019-09-13 04:20:14'),(14,'0000008','あべ　ひろし','93_hnspdR3xLj4SB04EsfPkoDHdua0ce59bhWriiLR4',1,2,0,'2019-09-13 04:27:31'),(17,'0000009','東野　八瀬','FcRZd-3FRJYDAc6l7rP20KwrlrLVQv1CUYqyTbnH-Ck',1,1,0,'2019-09-10 04:06:12'),(18,'0000010','浜辺　美波','c_XBIzdB5csSsix1KfVeRsZbltfuzOkbxBLKhywxfkU',1,2,0,'2019-09-10 04:16:09'),(19,'tesutoyou','テスト太郎','laz1nbBa0TmOzx6kKhnUOOEUQrkNI4ZLsuZpZ7I862k',2,3,0,'2019-09-13 02:43:06'),(20,'0000011','釘代','XeR1xU8pLTV7RmXEoGZzNU0K9YOr7CrFG3Uv3wb8270',1,1,0,'2019-09-18 06:42:51'),(21,'tesutoyou3','テストくん','kbTRQoI_fSDF8I32kSLeQ_NfBXqYjZYZ9tMThIXJogM',1,2,0,'2019-09-18 06:50:02'),(22,'0000013','テスト３','zPch67v9Sqv7DBAa4d9GpYXJRbdez5JkCAfKtVkCyFg',2,3,0,'2019-09-18 06:52:23'),(23,'0000014','テスト４','vh9HQ_IUiJF8T8z-yZOF7t8Dm1e3feuni4DV6gS8yx4',3,3,0,'2019-09-18 06:55:20'),(24,'0000015','テスト５','zD2lMx34jiMqwIheFC75wCfIFDI9-doc2nRbU49OlQ4',4,3,0,'2019-09-18 06:57:25'),(25,'0000016','テスト６','bOU2kbEmgI03RdcrkBY4TvChdABNydOrFR48gtoYa6U',2,4,0,'2019-09-18 05:41:15'),(26,'0000017','テスト７','Z6YZRXquPoaa8-fJIHhCSnczl8FSCpzsdv3lTug1ATc',3,4,0,'2019-09-18 05:48:03'),(27,'0000018','テスト８','UlL1LLeeInZ4PP3KUDBP7Qag6ryNvMOr_jqqxXksT8Y',4,4,0,'2019-09-18 05:51:41');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-30 13:11:27
